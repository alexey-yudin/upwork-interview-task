function memoize(fn) {
  const hashMap = {};

  return function() {
    const key = JSON.stringify(arguments);

    if (!(key in hashMap)) {
      hashMap[key] = fn.apply(this, arguments);
    }

    return hashMap[key];
  };
}

module.exports = memoize;
