/*
* Implement the "memoize" function, that caches the return value depending on the input.
Also write a test suite that tests that caching works as expected.

const sum = (a, b) => a + b;
const mul = (a, b) => a * b;
const memoize = () => ...;
const sumMemoized = memoize(sum);
const mulMemoized = memoize(mul);

sumMemoized(1, 3) === 4

mulMemoized(2, 4) === 8
* */

const memoize = require('../index');

describe('Memoize', () => {
  it('should be defined', () => {
    expect(memoize).toBeDefined();
  });

  it('should return a function', () => {
    const fn = (a, b) => a + b;
    expect(typeof memoize(fn)).toEqual('function');
  });

  describe('Returned function', () => {
    it('should be called with any amount of arguments', () => {
      function sum() {
        return Array.from(arguments).reduce((acc, current) => acc + current, 0);
      }
      const memoizedSum = memoize(sum);

      expect(memoizedSum()).toEqual(0);
      expect(memoizedSum(1)).toEqual(1);
      expect(memoizedSum(1, 2, 4)).toEqual(7);
    });

    it('should return the same result as the original', () => {
      const fn = jest.fn();
      const memoizedFn = memoize(fn);
      memoizedFn(1);
      memoizedFn(1);

      expect(fn).toHaveBeenCalledTimes(1);
    });
  });
});
